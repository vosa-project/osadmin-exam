#!/bin/bash
# Script for checking lab objectives

# Created by Roland Kaur
#
# Date - 2017-03-01
# Version - 0.0.1

LC_ALL=C

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/sshkeys"
	CheckFile2="/tmp/sshkeys2"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.0.2
	IP_to_SSH2=192.168.0.1

	# Time to sleep between running the check again
	Sleep=1

	# Objectice Obane ib VirtualTA
	Oname=examobjective

	# Step uname in VirtualTA
	Uname=sshkeys

}

# Test if action is successful

OSADMIN-EXAM () {

	counter=0

   	# Check if user has done the correct action
    	ssh student@$IP_to_SSH2 "ssh root@$IP_to_SSH 'hostname'" && ssh root@$IP_to_SSH "grep -i ed25519 /root/.ssh/authorized_keys" && ssh root@$IP_to_SSH "getent shadow root | grep \!"

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nKeys enabled!! Date: `date`\n" && touch $CheckFile

        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1

        	exit 0

    	else

        	echo -e "Keys not enabled! Date: `date`\n" >&2
		sleep $Sleep

    	fi

}

START

OSADMIN-EXAM

exit 0
