#!/bin/bash
# Script for checking lab objectives

# Created by Roland Kaur
#
# Date - 2017-03-01
# Version - 0.0.1

LC_ALL=C

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/scriptcreation"
	CheckFile2="/tmp/scriptcreation2"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.0.2

	# Time to sleep between running the check again
	Sleep=1

	# Objectice Obane ib VirtualTA
	Oname=examobjective

	# Step uname in VirtualTA
	Uname=scriptcreation

	if [ ! -f $CheckFile2 ]; then

		# Fetch step json entry
		curl -H "Content-Type: application/json" -X GET -d '{"api_key":"'"$TA_KEY"'", "lab":"'"$LAB_ID"'", "uname":"'"$Uname"'" }' $VIRTUALTA_HOSTNAME/api/v1/step > /tmp/nextstep

		# Parse step json entries into variables
		STEP_INST=$(cat /tmp/nextstep | jq '.instruction') && TEMP="${STEP_INST%\"}" && STEP_INST="${TEMP#\"}"
	        STEP_TITLE=$(cat /tmp/nextstep | jq '.title') && TEMP="${STEP_TITLE%\"}" && STEP_TITLE="${TEMP#\"}"
	        STEP_LONGTITLE=$(cat /tmp/nextstep | jq '.longTitle') && TEMP="${STEP_LONGTITLE%\"}" && STEP_LONGTITLE="${TEMP#\"}"
	 	STEP_MATERIAL=$(cat /tmp/nextstep | jq '.material') && TEMP="${STEP_MATERIAL%\"}" && STEP_MATERIAL="${TEMP#\"}"
	        STEP_WEB=$(cat /tmp/nextstep | jq '.web') && TEMP="${STEP_WEB%\"}" && STEP_WEB="${TEMP#\"}"
		STEP_COMPM=$(cat /tmp/nextstep | jq '.completionMethod') && TEMP="${STEP_COMPM%\"}" && STEP_COMPM="${TEMP#\"}"
		STEP_USRKEY=$(curl -H "Content-Type: application/json" -X GET -d '{"api_key":"'"$TA_KEY"'", "username":"'"$LAB_USERNAME"'"}' $VIRTUALTA_HOSTNAME/api/v1/user | jq '.key' | jq '.key') && TEMP="${STEP_USRKEY%\"}" && STEP_USRKEY="${TEMP#\"}"

		STEP_INST=${STEP_INST/XYZ/$FILENAME}

		# Replace step
		curl -H "Content-Type: application/json" -X PUT -d '{"api_key":"'"$TA_KEY"'", "lab": "'"$LAB_ID"'", "user":"'"$STEP_USRKEY"'", "uname":"'"$Uname"'", "instruction": "'"$STEP_INST"'"  }' $VIRTUALTA_HOSTNAME/api/v1/labuser_step

		touch $CheckFile2
	fi

}

# Test if action is successful

OSADMIN-EXAM () {

	counter=0

   	# Check if user has done the correct action
    	ssh root@$IP_to_SSH "test -e /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep '#!/bin/bash\|#!/bin/sh' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep 'apt' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep 'update' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep 'install' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep 'figlet' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep 'lolcat' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep 'clean' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "grep '\-y' /root/$FILENAME.sh" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "dpkg-query -l figlet" && counter=$(($counter + 1))
	ssh root@$IP_to_SSH "dpkg-query -l lolcat" && counter=$(($counter + 1))


   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $counter -eq 11 ]; then

        	echo -e "\nScript created!! Date: `date`\n" && touch $CheckFile

        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1

        	exit 0

    	else

        	echo -e "Skript has not been created! Date: `date`\n" >&2
		sleep $Sleep

    	fi

}

START

OSADMIN-EXAM

exit 0
