#!/bin/bash


# Removes 's from words and convert to lowercase
remove_apo () {
                        result=$(echo $1 | tr '[:upper:]' '[:lower:]')
                        if [[ $result == *"'s"* ]]; then
                                result=${result::-2}
                        fi
              }

# Name of the user to be created

USERNAME=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $USERNAME
export USERNAME=$result

# Name of the group the user belongs to

GROUPNAME=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $GROUPNAME
export GROUPNAME=$result

# Name of the script to be created

FILENAME=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $FILENAME
export FILENAME=$result

# Directories and subdirectories to be created + directory owner and group

DIRECTORY=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $DIRECTORY
export DIRECTORY=$result

DIROWNER=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $DIROWNER
export DIROWNER=$result

DIRGROUP=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $DIRGROUP
export DIRGROUP=$result

#i="0"
#
#while [ $i -lt 4 ]; do
#        DIR=$(/usr/bin/shuf -n1 /usr/share/dict/words)
#        remove_apo $DIR
#        DIR=$result
#
#        export DIRECTORY=$DIRECTORY/$DIR
#        i=$[$i+1]
#done

# Name of the environment variable to be created and user who it should be created to

ENVNAME=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $ENVNAME
export ENVNAME=$(echo $result | tr '[:lower:]' '[:upper:]')

ENVUSER=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $ENVUSER
export ENVUSER=$result

# Name of the script to be created

SCRIPTNAME=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $SCRIPTNAME
export SCRIPTNAME=$result
export SCRIPTHASH=$(sha256sum /var/www/osadminexam.lab/osadmin.sh | cut -f1 -d" ")
mv /var/www/osadminexam.lab/osadmin.sh /var/www/osadminexam.lab/$SCRIPTNAME.sh

# Size of the partitions to be created in Mbytes

PART1SIZE=$(( ( RANDOM % 350 )  + 1 ))
PART2SIZE=$(( 400 - $PART1SIZE ))

# Directory for disk mounting

MOUNTER=$(/usr/bin/shuf -n1 /usr/share/dict/words)
remove_apo $MOUNTER
export MOUNTER=$result

# Filesystem type for diskmgt

FILESYS="ext$(( ( RANDOM % 3 )  + 2 ))"
