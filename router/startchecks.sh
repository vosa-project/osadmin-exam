#!/bin/bash
#description "Checks starting service for System Compromised"

# Check if ssh connection works

sshconnect=10

while [ $counter -gt 0  ]; do
	ssh root@192.168.0.2 exit && break
	sleep 1
	$((counter--))
done

cd /root/osadmin-exam/router/

TEMPLATE="VirtualBox"
DMIDECODEVERSION="$(dmidecode -s bios-version | cut -d'-' -f1,2)"

if [ "$DMIDECODEVERSION" != "$TEMPLATE" ]; then
. /root/osadmin-exam/router/lab_init.sh
. /root/osadmin-exam/router/lab_variables.sh
export LAB_USERNAME
export TA_KEY
export VIRTUALTA_HOSTNAME
export LAB_ID

export USERNAME
export GROUPNAME
export FILENAME
export DIRECTORY
export DIROWNER
export DIRGROUP
export ENVNAME
export ENVUSER
export SCRIPTNAME
export SCRIPTHASH
export PART1SIZE
export PART2SIZE
export MOUNTER
export FILESYS

while true

do

/root/osadmin-exam/router/exam-sshkeys.sh || true
/root/osadmin-exam/router/exam-usercreation.sh || true
/root/osadmin-exam/router/exam-directorycreation.sh || true
/root/osadmin-exam/router/exam-envcreation.sh || true
/root/osadmin-exam/router/exam-scriptcreation.sh || true
/root/osadmin-exam/router/exam-scriptdl.sh || true
/root/osadmin-exam/router/exam-swappiness.sh || true
/root/osadmin-exam/router/exam-diskmgt.sh || true
/root/osadmin-exam/router/exam-fstab.sh || true

done

fi
